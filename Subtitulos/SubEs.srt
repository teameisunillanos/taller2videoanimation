1
00:00:01,870 --> 00:00:05,400
La decadencia y los fallos de las referencias web

2
00:00:05,930 --> 00:00:13,000
Las URL tienen una gran utilidad en la cotidianidad, en el caso de las referencias en artículos científicos, 

3 
00:00:13,300 --> 00:00:18,830
estas se utilizan para construir trabajos existentes, justificar las afirmaciones, 

4
00:00:19,400 --> 00:00:22,800
proporcionar el contexto que realiza la investigación, y presentar, 

5
00:00:23,500 --> 00:00:27,600
analizar y comparar diferentes enfoques o metodologías.

6
00:00:27,800 --> 00:00:34,270
Como en el caso de nuestro amigo, se pueden encontrar URLs rotas con facilidad, 

7
00:00:34,600 --> 00:00:38,800
comprometiendo seriamente las bases del discurso científico moderno.

8
00:00:39,330 --> 00:00:46,100
Para comprender más a detalle este problema debemos entender como funcionan las URL.

9
00:00:47,770 --> 00:00:54,500
Una URL es la representación de una cadena de "Localizadores uniformes de recursos", 

10
00:00:55,000 --> 00:00:58,000
con la cual se especifican los recursos de internet.

11
00:00:58,800 --> 00:01:05,000
A su vez son un subconjunto de los URI (Uniform Resource Identifiers)

12
00:01:05,200 --> 00:01:09,930
que proporcionan una identificación abstracta de una ubicación de recursos.

13
00:01:12,000 --> 00:01:17,700
Para acceder a las URL se utilizan diferentes tecnologías como por ejemplo

14
00:01:18,100 --> 00:01:21,730
http, ftp, mailto, entre otras. 

15
00:01:21,900 --> 00:01:24,670
Cualquier falla a lo largo de las diferentes tecnologías

16
00:01:24,800 --> 00:01:28,970
a menudo resultará en una solicitud fallida de una URL.

17
00:01:29,730 --> 00:01:34,770
Los errores más comunes al momento de realizar una solicitud URL son: 

18
00:01:34,970 --> 00:01:43,630
400 Bad request, 401 Unauthorized, 403 Forbidden, 404 Not Found, 

19
00:01:43,830 --> 00:01:49,270
500 Internal Server Error, 503 Service Unavailable,

20
00:01:49,470 --> 00:01:54,970
504 Gateway Time-out, 901 Host lookup failure.

21
00:01:56,020 --> 00:01:59,060
¿Qué tan común es encontrar referencias inaccesibles?

22
00:01:59,150 --> 00:02:00,000
Para responder a esta pregunta, 

23
00:02:01,170 --> 00:02:08,220
el Professor Diomidis Spinellis de la Universidad de Economía y Negocios de Atenas 

24
00:02:09,160 --> 00:02:12,128
realizó un estudio de las URLs caídas

25
00:02:13,130 --> 00:02:18,170
y posteriormente un análisis estadístico de los datos recolectados en función de su antigüedad,

26
00:02:18,190 --> 00:02:21,150
su dominio y el tipo de extensión.

27
00:02:22,090 --> 00:02:27,200
Se inspeccionaron en total 4224 URLs de 

28
00:02:28,130 --> 00:02:35,000
referencias de 2471 artículos científicos de los último 5 años 

29
00:02:35,140 --> 00:02:39,230
(desde 1995 hasta 1999)

30
00:02:40,070 --> 00:02:42,230
del material publicado disponible 

31
00:02:43,090 --> 00:02:49,120
en la Biblioteca Digital de la ACM y la IEEE Computer Society.

32
00:02:51,000 --> 00:02:57,000
Se encontró que 27% de las URLs eran inaccesibles,

33
00:02:58,000 --> 00:03:03,000
cerca del 50% fueron inaccesibles 4 años después de la fecha de publicación

34
00:03:03,500 --> 00:03:07,000
y un año después subió al 60%.