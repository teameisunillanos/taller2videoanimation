1
00:00:01,870 --> 00:00:05,400
referências Decay e falhas web

2
00:00:05,930 --> 00:00:13,000
URLs são muito úteis na vida cotidiana, no caso de referências em artigos científicos,

3
00:00:13,300 --> 00:00:18,830
Estes são usados ​​para construir postos de trabalho existentes, justificar reivindicações,

4
00:00:19,400 --> 00:00:22,800
fornecer contexto conduz a investigação, e no presente,

5
00:00:23,500 --> 00:00:27,600
analisar e comparar diferentes abordagens ou metodologias.

6
00:00:27,800 --> 00:00:34,270
Como no caso do nosso amigo, pode ser encontrada URLs facilmente quebrado,

7
00:00:34,600 --> 00:00:38,800
comprometer seriamente as bases do discurso científico moderno.

8
00:00:39,330 --> 00:00:46,100
Mais detalhes para compreender este problema, devemos entender como URLs trabalhar.

9
00:00:47,770 --> 00:00:54,500
A URL é uma representação de uma série de "Uniform Resource Locator"

10
00:00:55,000 --> 00:00:58,000
com que os recursos de Internet especificados.

11
00:00:58,800 --> 00:01:05,000
Por sua vez são um subconjunto das URIs (Uniform Resource Identificadores)

12
00:01:05,200 --> 00:01:09,930
que eles fornecem um resumo identificar uma localização de recursos.

13
00:01:12,000 --> 00:01:17,700
Para acessar a URL usado tecnologias diferentes, tais como

14
00:01:18,100 --> 00:01:21,730
http, ftp, mailto, entre outros.

15
00:01:21,900 --> 00:01:24,670
Qualquer falha ao longo das diferentes tecnologias

16
00:01:24,800 --> 00:01:28,970
É, muitas vezes, resultar em um pedido falhou para uma URL.

17
00:01:29,730 --> 00:01:34,770
Os erros mais comuns ao fazer um pedido de URL são:

18
00:01:34,970 --> 00:01:43,630
400 Bad Pedido 401 Unauthorized 403 Proibido, 404 Not Found,

19
00:01:43,830 --> 00:01:49,270
500 Internal Server Error, 503 Service Unavailable,

20
00:01:49,470 --> 00:01:54,970
504 gateway Time-out, 901 falha de pesquisa de anfitrião.

21
00:01:56,020 --> 00:01:59,060
Como é comum referências inacessíveis encontrar?

22
00:01:59,150 --> 00:02:00,000
Para responder a esta pergunta,

23
00:02:01,170 --> 00:02:08,220
Professor Diomidis Spinellis Universidade de Economia e Negócios de Atenas

24
00:02:09,160 --> 00:02:12,128
Ele realizou um estudo de URLs caídos

25
00:02:13,130 --> 00:02:18,170
e em seguida uma análise estatística dos dados recolhidos em função da sua idade,

26
00:02:18,190 --> 00:02:21,150
seu domínio e tipo de extensão.

27
00:02:22,090 --> 00:02:27,200
Eles foram inspecionados em 4224 Total de URLs

28
00:02:28,130 --> 00:02:35,000
2471 artigos científicos referencia os últimos 5 anos

29
00:02:35,140 --> 00:02:39,230
(1995 a 1999)

30
00:02:40,070 --> 00:02:42,230
material publicado disponíveis

31
00:02:43,090 --> 00:02:49,120
na Biblioteca Digital ACM e IEEE Computer Society.

32
00:02:51,000 --> 00:02:57,000
Verificou-se que 27% dos URLs eram inacessíveis,

33
00:02:58,000 --> 00:03:03,000
cerca de 50% eram inacessíveis 4 anos após a data de publicação

34
00:03:03,500 --> 00:03:07,000
e um ano depois subiu para 60%.

