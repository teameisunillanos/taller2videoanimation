1
00:00:01,870 --> 00:00:05,400
Decay und Ausfälle Webreferenzen

2
00:00:05,930 --> 00:00:13,000
URLs sind sehr nützlich im Alltag, im Fall von Artikeln in wissenschaftlichen Arbeiten,

3
00:00:13,300 --> 00:00:18,830
Diese werden verwendet, um bestehende Arbeitsplätze zu bauen, rechtfertigen Ansprüche,

4
00:00:19,400 --> 00:00:22,800
liefern Zusammenhang die Durchführung der Untersuchung, und Gegenwart,

5
00:00:23,500 --> 00:00:27,600
analysieren und unterschiedliche Ansätze oder Methoden vergleichen.

6
00:00:27,800 --> 00:00:34,270
Wie im Fall von unserem Freund, können Sie leicht gebrochen URLs gefunden werden,

7
00:00:34,600 --> 00:00:38,800
die Grundlagen der modernen wissenschaftlichen Diskurs ernsthaft zu gefährden.

8
00:00:39,330 --> 00:00:46,100
Weitere Einzelheiten dieses Problem zu verstehen, müssen wir verstehen, wie URLs arbeiten.

9
00:00:47,770 --> 00:00:54,500
Eine URL ist eine Darstellung einer Reihe von „Uniform Resource Locators“

10
00:00:55,000 --> 00:00:58,000
mit denen die angegebenen Internet-Ressourcen.

11
00:00:58,800 --> 00:01:05,000
Wiederum ist eine Teilmenge von URIs (Uniform Resource Identifiers)

12
00:01:05,200 --> 00:01:09,930
die sie bieten eine abstrakte eine Position von Ressourcen zu identifizieren.

13
00:01:12,000 --> 00:01:17,700
Um die URL unterschiedliche Technologien verwendet zugreifen wie

14
00:01:18,100 --> 00:01:21,730
http, ftp, mailto, unter anderem.

15
00:01:21,900 --> 00:01:24,670
Jeder Fehler entlang der verschiedenen Technologien

16
00:01:24,800 --> 00:01:28,970
Es wird oft zu einem gescheiterten Antrag auf eine URL.

17
00:01:29,730 --> 00:01:34,770
Die häufigsten Fehler, wenn eine URL-Anforderung zu machen sind:

18
00:01:34,970 --> 00:01:43,630
400 Bad Request 401 Unauthorized 403 Verboten, 404 Not Found,

19
00:01:43,830 --> 00:01:49,270
500 Internal Server Error, 503 Dienst nicht verfügbar,

20
00:01:49,470 --> 00:01:54,970
504 Gateway-Time-out, 901 Host-Lookup-Fehler.

21
00:01:56,020 --> 00:01:59,060
Wie häufig ist unzugänglich finden Referenzen?

22
00:01:59,150 --> 00:02:00,000
Zur Beantwortung dieser Frage,

23
00:02:01,170 --> 00:02:08,220
Professor Diomidis Spinellis University of Economics and Business Athen

24
00:02:09,160 --> 00:02:12,128
Er führte eine Studie des gefallenen URLs

25
00:02:13,130 --> 00:02:18,170
und dann eine statistische Analyse der gesammelten Daten je nach Alter,

26
00:02:18,190 --> 00:02:21,150
seine Domäne und die Art der Erweiterung.

27
00:02:22,090 --> 00:02:27,200
Sie wurden in 4224 insgesamt URLs geprüft

28
00:02:28,130 --> 00:02:35,000
2471 wissenschaftliche Artikel verweist auf die letzten 5 Jahre

29
00:02:35,140 --> 00:02:39,230
(1995-1999)

30
00:02:40,070 --> 00:02:42,230
veröffentlichtes Material verfügbar

31
00:02:43,090 --> 00:02:49,120
in der Digitalen Bibliothek ACM und der IEEE Computer Society.

32
00:02:51,000 --> 00:02:57,000
Es wurde festgestellt, dass 27% der URLs waren nicht zugänglich,

33
00:02:58,000 --> 00:03:03,000
etwa 50% war nicht zugänglich 4 Jahre nach dem Datum der Veröffentlichung

34
00:03:03,500 --> 00:03:07,000
und ein Jahr später stieg auf 60%.

