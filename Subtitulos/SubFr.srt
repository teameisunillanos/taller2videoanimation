1
00:00:01,870 --> 00:00:05,400
Decay et échecs références web

2
00:00:05,930 --> 00:00:13,000
URL sont très utiles dans la vie quotidienne, dans le cas des références dans des articles scientifiques,

3
00:00:13,300 --> 00:00:18,830
Ceux-ci sont utilisés pour créer des emplois existants, justifier les réclamations,

4
00:00:19,400 --> 00:00:22,800
fournir un contexte qui mène l'enquête, et présente,

5
00:00:23,500 --> 00:00:27,600
analyser et comparer les différentes approches ou méthodologies.

6
00:00:27,800 --> 00:00:34,270
Comme dans le cas de notre ami, vous pouvez être trouvé facilement les URL cassés,

7
00:00:34,600 --> 00:00:38,800
compromettre sérieusement les fondements du discours scientifique moderne.

8
00:00:39,330 --> 00:00:46,100
Plus de détails pour comprendre ce problème, nous devons comprendre comment fonctionnent les URL.

9
00:00:47,770 --> 00:00:54,500
Une URL est une représentation d'une chaîne de « localisateurs de ressources uniformes »

10
00:00:55,000 --> 00:00:58,000
avec laquelle les ressources Internet spécifiées.

11
00:00:58,800 --> 00:01:05,000
À leur tour sont un sous-ensemble des URI (Uniform Resource Identifiers)

12
00:01:05,200 --> 00:01:09,930
qu'ils fournissent un résumé identifiant un emplacement des ressources.

13
00:01:12,000 --> 00:01:17,700
Pour accéder à l'URL utilisée différentes technologies telles que

14
00:01:18,100 --> 00:01:21,730
http, ftp, mailto, entre autres.

15
00:01:21,900 --> 00:01:24,670
Tout manquement long des différentes technologies

16
00:01:24,800 --> 00:01:28,970
Il se traduit souvent par une requête a échoué pour une URL.

17
00:01:29,730 --> 00:01:34,770
Les erreurs les plus courantes lors d'une demande d'URL sont les suivantes:

18
00:01:34,970 --> 00:01:43,630
400 Bad Request 401 Unauthorized 403 Interdit, 404 Not Found,

19
00:01:43,830 --> 00:01:49,270
500 Internal Server Error, 503 Service non disponible,

20
00:01:49,470 --> 00:01:54,970
504 Passerelle Time-out, 901 échec de la recherche de l'hôte.

21
00:01:56,020 --> 00:01:59,060
Quelle est la fréquence des références FIND inaccessibles?

22
00:01:59,150 --> 00:02:00,000
Pour répondre à cette question,

23
00:02:01,170 --> 00:02:08,220
Professeur Diomidis Spinellis Université d'économie et d'affaires d'Athènes

24
00:02:09,160 --> 00:02:12,128
Il a mené une étude des URL déchus

25
00:02:13,130 --> 00:02:18,170
puis une analyse statistique des données recueillies en fonction de leur âge,

26
00:02:18,190 --> 00:02:21,150
son domaine et le type d'extension.

27
00:02:22,090 --> 00:02:27,200
Ils ont été inspectés en 4224 URL au total

28
00:02:28,130 --> 00:02:35,000
2471 articles scientifiques références les 5 dernières années

29
00:02:35,140 --> 00:02:39,230
(1995 à 1999)

30
00:02:40,070 --> 00:02:42,230
matériel publié disponible

31
00:02:43,090 --> 00:02:49,120
dans la bibliothèque numérique ACM et l'IEEE Computer Society.

32
00:02:51,000 --> 00:02:57,000
On a constaté que 27% des URL étaient inaccessibles,

33
00:02:58,000 --> 00:03:03,000
environ 50% étaient inaccessibles 4 ans après la date de publication

34
00:03:03,500 --> 00:03:07,000
et un an plus tard a augmenté à 60%.

