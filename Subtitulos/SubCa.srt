1
00:00:01,870 --> 00:00:05,400
La decadència i les fallades de les referències web

2
00:00:05,930 --> 00:00:13,000
Les URL tenen una gran utilitat en la quotidianitat, en el cas de les referències a articles científics,

3
00:00:13,300 --> 00:00:18,830
aquestes s'utilitzen per construir treballs existents, justificar les afirmacions,

4
00:00:19,400 --> 00:00:22,800
proporcionar el context que realitza la investigació, i presentar,

5
00:00:23,500 --> 00:00:27,600
analitzar i comparar diferents enfocaments o metodologies.

6
00:00:27,800 --> 00:00:34,270
Com en el cas del nostre amic, es poden trobar URL trencades amb facilitat,

7
00:00:34,600 --> 00:00:38,800
comprometent seriosament les bases del discurs científic modern.

8
00:00:39,330 --> 00:00:46,100
Per comprendre més a detall aquest problema hem d'entendre com funcionen les URL.

9
00:00:47,770 --> 00:00:54,500
Un URL és la representació d'una cadena de "Localitzadors uniformes de recursos",

10
00:00:55,000 --> 00:00:58,000
amb la qual s'especifiquen els recursos d'internet.

11
00:00:58,800 --> 00:01:05,000
Al seu torn són un subconjunt dels URI (Uniform Resource Identifiers)

12
00:01:05,200 --> 00:01:09,930
que proporcionen una identificació abstracta d'una ubicació de recursos.

13
00:01:12,000 --> 00:01:17,700
Per accedir a les URL s'utilitzen diferents tecnologies com ara

14
00:01:18,100 --> 00:01:21,730
http, ftp, mailto, entre d'altres.

15
00:01:21,900 --> 00:01:24,670
Qualsevol falla al llarg de les diferents tecnologies

16
00:01:24,800 --> 00:01:28,970
sovint resultarà en una sol·licitud fallida d'una URL.

17
00:01:29,730 --> 00:01:34,770
Els errors més comuns al moment de realitzar una sol·licitud URL són:

18
00:01:34,970 --> 00:01:43,630
400 Bad request, 401 Unauthorized, 403 Forbidden, 404 Not Found,

19
00:01:43,830 --> 00:01:49,270
500 Internal Server Error, 503 Service Unavailable,

20
00:01:49,470 --> 00:01:54,970
504 Gateway Time-out, 901 Host lookup failure.

21
00:01:56,020 --> 00:01:59,060
Què tan comú és trobar referències inaccessibles?

22
00:01:59,150 --> 00:02:00,000
Per respondre a aquesta pregunta,

23
00:02:01,170 --> 00:02:08,220
el Professor Diomidis Spinellis de la Universitat d'Economia i Negocis d'Atenes

24
00:02:09,160 --> 00:02:12,128
va realitzar un estudi de les URL caigudes

25
00:02:13,130 --> 00:02:18,170
i posteriorment una anàlisi estadística de les dades recollides en funció de la seva antiguitat,

26
00:02:18,190 --> 00:02:21,150
el seu domini i el tipus d'extensió.

27
00:02:22,090 --> 00:02:27,200
Es van inspeccionar en total 4224 URL de

28
00:02:28,130 --> 00:02:35,000
referències de 2471 articles científics dels últim 5 anys

29
00:02:35,140 --> 00:02:39,230
(Des de 1995 fins 1999)

30
00:02:40,070 --> 00:02:42,230
del material publicat disponible

31
00:02:43,090 --> 00:02:49,120
a la Biblioteca Digital de l'ACM i la IEEE Computer Society.

32
00:02:51,000 --> 00:02:57,000
Es va trobar que el 27% de les URL eren inaccessibles,

33
00:02:58,000 --> 00:03:03,000
prop del 50% van ser inaccessibles 4 anys després de la data de publicació

34
00:03:03,500 --> 00:03:07,000
i un any després va pujar al 60%.

