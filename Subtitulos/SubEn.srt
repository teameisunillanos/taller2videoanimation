1
00:00:01,870 --> 00:00:05,400
The decay and failures of web reference

2
00:00:05,930 --> 00:00:13,000
URLs have a great utility in everyday life, in the case of references in scientific articles,

3 
00:00:13,300 --> 00:00:18,830
these are used to build the existing tasks, to justify the affirmations, 

4
00:00:19,400 --> 00:00:22,800
to provide the context of the research, and to present, 

5
00:00:23,500 --> 00:00:27,600
analyze and compare different approaches or methodologies.

6
00:00:27,800 --> 00:00:34,270
As in the case of our friend, you can find broken URLs easily,

7
00:00:34,600 --> 00:00:38,800
seriously compromising the basis of modern scientific discourse.

8
00:00:39,330 --> 00:00:46,100
To understand this problem in detail, we must understand how URLs work.

9
00:00:47,770 --> 00:00:54,500
A URL is the representation of a string of "Uniform Resource Locators", 

10
00:00:55,000 --> 00:00:58,000
which specifies the internet resources. 

11
00:00:58,800 --> 00:01:05,000
In turn, they are a subset of Uniform Resource Identifiers (URIs) 

12
00:01:05,200 --> 00:01:09,930
that provide an abstract identification of a resource location.

13
00:01:12,000 --> 00:01:17,700
To access the URLs are used different technologies such as 

14
00:01:18,100 --> 00:01:21,730
http, ftp, mailto, among others.

15
00:01:21,900 --> 00:01:24,670
Any failure across different technologies 

16
00:01:24,800 --> 00:01:28,970
will often result in a failed request for a URL.

17
00:01:29,730 --> 00:01:34,770
The most common mistakes at the time of making a URL request are: 

18
00:01:34,970 --> 00:01:43,630
400 Bad request, 401 Unauthorized, 403 Forbidden, 404 Not Found, 

19
00:01:43,830 --> 00:01:49,270
500 Internal Server Error, 503 Service Unavailable, 

20
00:01:49,470 --> 00:01:55,000
504 Gateway Time-out, 901 Host lookup failure.

21
00:01:56,020 --> 00:01:59,060
How common it is to find inaccessible references?

22
00:01:59,150 --> 00:02:00,000
To answer this question

23
00:02:01,170 --> 00:02:08,220
Professor Diomidis Spinellis of the University of Economics and Business of Athens 

24
00:02:09,160 --> 00:02:12,128
conducted a study of the fallen URLs 

25
00:02:13,130 --> 00:02:18,170
and later a statistical analysis of the data collected according to their age

26
00:02:18,190 --> 00:02:21,150
their domain and the type of extension

27
00:02:22,090 --> 00:02:27,200
A total of 4224 reference URLs of 

28
00:02:28,130 --> 00:02:35,000
2471 scientific papers from the last 5 years

29
00:02:35,140 --> 00:02:39,230
(from 1995 to 1999) 

30
00:02:40,070 --> 00:02:42,230
were inspected for material published 

31
00:02:43,090 --> 00:02:49,120
in the ACM digital Library and the IEEE Computer Society.

32
00:02:51,000 --> 00:02:57,000
It was found that 27% of the URLs were inaccessible

33
00:02:58,000 --> 00:03:03,000
about 50% were inaccessible 4 years after the publication date 

34
00:03:03,500 --> 00:03:07,000
and one year later rose to 60%

