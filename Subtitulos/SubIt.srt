1
00:00:01,870 --> 00:00:05,400
Decay e fallimenti riferimenti web

2
00:00:05,930 --> 00:00:13,000
Gli URL sono molto utili nella vita di tutti i giorni, nel caso di riferimenti a pubblicazioni scientifiche,

3
00:00:13,300 --> 00:00:18,830
Questi sono usati per costruire posti di lavoro esistenti, giustificare pretese,

4
00:00:19,400 --> 00:00:22,800
fornire un contesto che conduce le indagini, e il presente,

5
00:00:23,500 --> 00:00:27,600
analizzare e confrontare gli approcci o metodologie diverse.

6
00:00:27,800 --> 00:00:34,270
Come nel caso del nostro amico, si può essere trovato URL facilmente rotto,

7
00:00:34,600 --> 00:00:38,800
compromettere seriamente le basi della moderna discorso scientifico.

8
00:00:39,330 --> 00:00:46,100
Più in dettaglio per capire questo problema dobbiamo capire come funzionano gli URL.

9
00:00:47,770 --> 00:00:54,500
Un URL è una rappresentazione di una serie di "Uniform Resource Locator"

10
00:00:55,000 --> 00:00:58,000
con cui le risorse Internet specificato.

11
00:00:58,800 --> 00:01:05,000
A loro volta sono un sottoinsieme di URI (Uniform Resource Identifier)

12
00:01:05,200 --> 00:01:09,930
che essi forniscono un abstract che identifica una posizione di risorse.

13
00:01:12,000 --> 00:01:17,700
Per accedere alla URL utilizzato diverse tecnologie, come

14
00:01:18,100 --> 00:01:21,730
http, ftp, mailto, tra gli altri.

15
00:01:21,900 --> 00:01:24,670
Qualsiasi guasto lungo le diverse tecnologie

16
00:01:24,800 --> 00:01:28,970
Sarà spesso sfociano in una richiesta non riuscita per un URL.

17
00:01:29,730 --> 00:01:34,770
Gli errori più comuni quando si effettua una richiesta URL sono:

18
00:01:34,970 --> 00:01:43,630
400 Bad Request 401 non autorizzato 403 Forbidden, 404 Not Found,

19
00:01:43,830 --> 00:01:49,270
500 Internal Server Error, 503 Servizio non disponibile,

20
00:01:49,470 --> 00:01:54,970
504 Gateway Timeout, 901 errore di ricerca Host.

21
00:01:56,020 --> 00:01:59,060
Come comune è riferimenti find inaccessibili?

22
00:01:59,150 --> 00:02:00,000
Per rispondere a questa domanda,

23
00:02:01,170 --> 00:02:08,220
Il professor Diomidis Spinellis Università di Economia e Commercio di Atene

24
00:02:09,160 --> 00:02:12,128
Egli ha condotto uno studio di URL caduti

25
00:02:13,130 --> 00:02:18,170
e poi un'analisi statistica dei dati raccolti a seconda della loro età,

26
00:02:18,190 --> 00:02:21,150
suo dominio e il tipo di estensione.

27
00:02:22,090 --> 00:02:27,200
Sono stati ispezionati nel 4224 URL totali

28
00:02:28,130 --> 00:02:35,000
2471 articoli scientifici fa riferimento negli ultimi 5 anni

29
00:02:35,140 --> 00:02:39,230
(1995-1999)

30
00:02:40,070 --> 00:02:42,230
materiale pubblicato disponibili

31
00:02:43,090 --> 00:02:49,120
nella Biblioteca ACM digitale e IEEE Computer Society.

32
00:02:51,000 --> 00:02:57,000
Si è riscontrato che il 27% di URL erano inaccessibili,

33
00:02:58,000 --> 00:03:03,000
circa il 50% erano inaccessibili 4 anni dopo la data di pubblicazione

34
00:03:03,500 --> 00:03:07,000
e un anno dopo è salito al 60%.

